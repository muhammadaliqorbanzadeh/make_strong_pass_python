from random import choice
import string as s

while True:
    length = int(input('pleaser enter how many char you want for your pass? :'))

    pattern = s.ascii_letters + s.digits + s.punctuation

    password = ''.join([choice(pattern) for i in range(length)])

    print(password)

    while True:
        ask_for_more = input('you want more password? (Y/n) : ').lower()

        if ask_for_more == 'n' or ask_for_more == 'y':
            break

    if ask_for_more == 'n':
        break